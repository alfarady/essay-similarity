<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('kelas/{kelas:slug}/home', 'HomeController@home')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('users', 'UserController');
    Route::resource('kelas.materi', 'MateriController')->parameters([
        'kelas' => 'kelas:slug',
    ]);
    Route::resource('kelas.video', 'VideoController')->parameters([
        'kelas' => 'kelas:slug',
    ]);
    Route::post('kelas/{kelas:slug}/task/{id}/submit', 'TaskController@submit')->name('task.submit');
    Route::resource('kelas.task', 'TaskController')->parameters([
        'kelas' => 'kelas:slug',
    ]);
    Route::get('kelas/{kelas:slug}/people', 'KelasController@people')->name('kelas.people');
    Route::get('kelas/{kelas:slug}/diskusi', 'KelasController@diskusi')->name('kelas.diskusi');
    Route::get('kelas/join/create', 'KelasController@cjoin')->name('join.create');
    Route::post('kelas/join', 'KelasController@join')->name('join.store');
    Route::resource('kelas', 'KelasController')->parameters([
        'kelas' => 'kelas:slug',
    ]);
    Route::get('kelas/{kelas:slug}/soal/{id}/essayshow', 'SoalController@essay')->name('soal.showEssay');
    Route::post('kelas/{kelas:slug}/soal/exam/answer', 'SoalController@storeAnswer')->name('soal.storeAnswer');
    Route::post('kelas/{kelas:slug}/soal/exam/finish', 'SoalController@examFinish')->name('soal.examFinish');
    Route::post('kelas/{kelas:slug}/soal/{id}/item', 'SoalController@storeItem')->name('soal.storeItem');
    Route::post('kelas/{kelas:slug}/soal/{id}/exam', 'SoalController@examStart')->name('soal.examStart');
    Route::resource('kelas.soal', 'SoalController')->parameters([
        'kelas' => 'kelas:slug',
    ]);
    Route::get('home/notifications', 'HomeController@loadMoreNotifications');
    Route::post('kelas/{kelas:slug}/home/feed', 'HomeController@feed')->name('home.feed');
    Route::post('kelas/{kelas:slug}/home/reply', 'HomeController@reply')->name('home.reply');

    Route::get('kelas/{kelas:slug}/activity', 'ReportController@activity')->name('report.activity');
    Route::post('kelas/{kelas:slug}/nilai', 'ReportController@saveNilai')->name('report.saveNilai');
    Route::get('kelas/{kelas:slug}/nilai', 'ReportController@nilai')->name('report.nilai');
    Route::get('kelas/{kelas:slug}/nilai/{id}', 'ReportController@nilaiDetail')->name('report.nilaiDetail');

    Route::get('setting', 'HomeController@setting')->name('home.setting');
    Route::post('setting', 'HomeController@updateSetting')->name('home.updateSetting');
    Route::post('password', 'HomeController@updatePassword')->name('home.updatePassword');
});
