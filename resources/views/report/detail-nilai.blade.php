@extends('layouts.app')

@section('header')
    @include('kelas.partials.header')
@endsection

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title d-flex justify-content-center mb-0">Nilai {{ $siswa->name }}</h2>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Data list view starts -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table id="table-nilai" class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Soal</th>
                                            @if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('mentor'))
                                                <th>Kunci Jawaban</th>
                                            @endif
                                            <th>Jawaban Siswa</th>
                                            <th>Similarity</th>
                                            <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $value)
                                            @php
                                                $score = $value->score ?? 0;
                                                $score = round($score, 2);
                                                $human_score = 0;
                                                if($score >= 0 && $score <= 0.25) $human_score = 0;
                                                else if($score > 0.25 && $score <= 0.5) $human_score = 1;
                                                else if($score > 0.5 && $score <= 0.75) $human_score = 2;
                                                else if($score > 0.75 && $score <= 1) $human_score = 3;
                                            @endphp
                                            <tr>
                                                <td>{{ $value->soal_item->nomor ?? '0' }}</td>
                                                <td>{{ $value->soal_item->question ?? '' }}</td>
                                                @if (auth()->user()->hasRole('admin') || auth()->user()->hasRole('mentor'))
                                                    <td>{{ $value->soal_item->kj ?? '' }}</td>
                                                @endif
                                                <td>{{ $value->answer ?? '' }}</td>
                                                <td>{{ $score }}</td>
                                                <td>{{ $human_score }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
