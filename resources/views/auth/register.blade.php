@extends('layouts.auth')

@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="row flexbox-container">
                <div class="col-xl-8 col-10 d-flex justify-content-center">
                    <div class="card bg-authentication rounded-0 mb-0">
                        <div class="row m-0">
                            <div class="col-lg-6 col-12 p-0">
                                <div class="card rounded-0 mb-0 p-2">
                                    <div class="card-header pt-50 pb-1">
                                        <div class="card-title">
                                            <h4 class="mb-0">Sign Up</h4>
                                        </div>
                                    </div>
                                    <p class="px-2">Isi form di bawah ini untuk membuat akun baru.</p>
                                    <div class="card-content">
                                        <div class="card-body pt-0">
                                            <form action="{{ route('register') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-label-group">
                                                    <input oninvalid="this.setCustomValidity('Mohon diisi dengan lengkap')"
                                                    oninput="this.setCustomValidity('')" type="text" id="inputName" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required>
                                                    <label for="inputName">Name</label>
                                                    @error('name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                    <div class="form-control-position">
                                                        <i class="feather icon-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-label-group">
                                                    <input oninvalid="this.setCustomValidity('Mohon diisi dengan lengkap')"
                                                    oninput="this.setCustomValidity('')" type="email" id="inputEmail" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required>
                                                    <label for="inputEmail">Email</label>
                                                    @error('email')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                    <div class="form-control-position">
                                                        <i class="feather icon-mail"></i>
                                                    </div>
                                                </div>
                                                <div class="form-label-group">
                                                    <fieldset class="form-group">
                                                        <select oninvalid="this.setCustomValidity('Mohon diisi dengan lengkap')"
                                                        oninput="this.setCustomValidity('')" required class="form-control" id="status" name="status">
                                                            <option disabled selected value="">Pilih</option>
                                                            <option value="student">Siswa</option>
                                                            <option value="mentor">Guru</option>
                                                        </select>
                                                    </fieldset>
                                                    <label for="status">Status</label>
                                                </div>
                                                <div class="form-label-group">
                                                    <input oninvalid="this.setCustomValidity('Mohon diisi dengan lengkap')"
                                                    oninput="this.setCustomValidity('')" type="password" id="inputPassword" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
                                                    <label for="inputPassword">Password</label>
                                                    @error('password')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                    <div class="form-control-position">
                                                        <i class="feather icon-lock"></i>
                                                    </div>
                                                </div>
                                                <div class="form-label-group">
                                                    <input oninvalid="this.setCustomValidity('Mohon diisi dengan lengkap')"
                                                    oninput="this.setCustomValidity('')" type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                                                    <label for="password_confirmation">Confirm Password</label>
                                                    <div class="form-control-position">
                                                        <i class="feather icon-lock"></i>
                                                    </div>
                                                </div>
                                                <div class="form-label-group">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="inputGroupFile01" name="image" required>
                                                            <label class="custom-file-label" for="inputGroupFile01">Choose photo</label>
                                                            @error('image')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                </div>
                                                <a href="{{ route('login') }}" class="btn btn-outline-primary float-left btn-inline mb-50">Login</a>
                                                <button type="submit" class="btn btn-primary float-right btn-inline mb-50">Register</a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="login-footer">
                                        <div class="divider">
                                            <div class="divider-text">Copyright &copy; 2020</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 d-lg-block d-none text-center align-self-center pl-0 pr-3 py-0">
                                <img src="{{ asset('app-assets/images/pages/register.jpg') }}" alt="branding logo">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
@endsection
