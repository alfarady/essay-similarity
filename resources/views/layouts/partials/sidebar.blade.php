<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('dashboard') }}">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0" style="font-size: 14px">AES</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item {{ request()->is('dashboard') ? ' active' : '' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
            </li>
            @if (auth()->user()->hasRole('admin'))
            <li class=" nav-item"><a href="#"><i class="fa fa-database"></i><span class="menu-title" data-i18n="Data Master">Data Master</span></a>
                <ul class="menu-content">
                    <li class="{{ request()->is('kelas') ? ' active is-shown' : '' }}">
                        <a href="{{ route('kelas.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Data Kelas">Data Kelas</span></a>
                    </li>
                    <li class="{{ request()->type == 'mentor' ? ' active is-shown' : '' }}">
                        <a href="{{ route('users.index', ['type' => 'mentor']) }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Data Guru">Data Guru</span></a>
                    </li>
                    <li class="{{ request()->type == 'student' ? ' active is-shown' : '' }}">
                        <a href="{{ route('users.index', ['type' => 'student']) }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Data Siswa">Data Siswa</span></a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
</div>
