@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/users.css') }}">
@endsection

@section('header')
    @include('kelas.partials.header')
@endsection

@section('content')
<div class="content-header row">
</div>
<div class="content-body">
    <div id="user-profile">
        <div class="row">
            <div class="col-12">
                <div class="profile-header mb-2">
                    <div class="relative">
                        <div class="cover-container">
                            <img class="img-fluid bg-cover rounded-0 w-100" style="height: 300px" src="https://img.lovepik.com//back_pic/05/61/36/115b4890e8eab7f.jpg_wh300.jpg" alt="User Profile Image">
                            <div class="card-img-overlay overflow-hidden">
                                <h1 class="card-text text-white ml-3" style="font-size: 3rem">{{ $kelas->name ?? '' }}</h1>
                            </div>
                        </div>
                        <div class="profile-img-container d-flex align-items-center justify-content-between">
                            <img style="background: white" src="{{ auth()->user()->image ? asset('uploads/images/'.auth()->user()->image) : asset('uploads/images/profile.png') }}" class="rounded-circle img-border box-shadow-1" alt="Card image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section id="profile-info">
            <div class="row mt-3">
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4>MENU</h4>
                        </div>
                        <div class="list-group list-group-filters font-medium-1">
                            <a href="{{ route('kelas.diskusi', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-message-circle mr-50"></i> Diskusi</a>
                            <div class="dropdown">
                                <a class="list-group-item list-group-item-action border-0" type="button" id="materi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="font-medium-5 feather icon-file mr-50"></i> Materi</a>
                                <div class="dropdown-menu" aria-labelledby="materi">
                                    <a href="{{ route('kelas.materi.index', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-file mr-50"></i> PPT/PDF</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="{{ route('kelas.video.index', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-film mr-50"></i> VIDEO</a>
                                </div>
                            </div>
                            @include('kelas.partials.dropdown')
                            <a href="{{ route('report.nilai', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-server mr-50"></i> Hasil Kinerja</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <a href="{{ route('report.activity', [$kelas->slug]) }}"><h4 class="mb-2">Aktivitas Terbaru</h4></a>
                            @if(!auth()->user()->hasRole('student'))
                            <a type="button"><h5 class="mb-2 text-bold-500">Kode Kelas : {{ $kelas->code ?? '' }}</h5></a>
                            @endif
                        </div>
                    </div>
                    @foreach ($data as $value)
                    <div class="alert alert-light" role="alert">
                        <h4 class="alert-heading text-dark">{{ $value->causer->name }} | {{ date('d/m/Y H:i:s', strtotime($value->created_at)) }}</h4>
                        <p class="text-dark">{{ $value->description ?? '' }}</p>
                    </div>
                    @endforeach
                </div>
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4>Materi Terbaru</h4>
                        </div>
                        <div class="card-body">
                            @foreach ($materi as $value)
                                <div class="d-flex justify-content-start align-items-center mb-1">
                                    <div class="user-page-info">
                                        <h6 class="mb-0">{{ $value->name }}</h6>
                                        <span class="font-small-2">{{ $value->created_at->diffForHumans() }}</span>
                                    </div>
                                    <button type="button" data-link="{{ asset('uploads/file/'.$value->file) }}" data-href="{{ route('kelas.materi.show', [$kelas->slug, $value->id]) }}" class="btn btn-primary btn-icon ml-auto action-show"><i class="fa fa-download"></i></button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4>Vidio Terbaru</h4>
                        </div>
                        <div class="card-body">
                            @foreach ($video as $value)
                                <div class="d-flex justify-content-start align-items-center mb-1">
                                    <div class="user-page-info">
                                        <h6 class="mb-0">{{ $value->name }}</h6>
                                        <span class="font-small-2">{{ $value->created_at->diffForHumans() }}</span>
                                    </div>
                                    <a type="button" href="{{ route('kelas.video.index', [$kelas->slug]) }}" class="btn btn-primary btn-icon ml-auto text-white"><i class="fa fa-play-circle"></i></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade action-modal" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true"></div>
@endsection

@section('js')
    <script src="{{ asset('app-assets/js/scripts/pages/user-profile.js') }}"></script>
    <script>
        $('.action-show').on("click",function(e){
            e.stopPropagation();
            const link = $(this).data('link');
            $.ajax({
                url: $(this).data('href'),
                dataType: "json",
                success: function(data) {
                    window.open(link,'_blank');
                }
            })
        });
    </script>
    <script>
        $('.datatable').on('click', '.btn-modal', function(e){
            var t = $('.action-modal');
            $.ajax({
                url: $(this).data('href'),
                dataType: "html",
                success: function(e) {
                    $(t).html(e).modal("show")
                }
            })
        })
    </script>
@endsection
