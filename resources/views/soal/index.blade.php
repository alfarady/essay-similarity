@extends('layouts.app')

@section('header')
    @include('kelas.partials.header')
@endsection

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title d-flex justify-content-center mb-0">Data Soal</h2>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Data list view starts -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-outline-primary btn-modal" data-href="{{ route('kelas.soal.create', [$kelas->slug]) }}"><i class='feather icon-plus'></i> Tambah</button>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th>Soal</th>
                                            <th>Waktu</th>
                                            <th>Tgl Mulai</th>
                                            <th>Tgl Deadline</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $value)
                                            <tr>
                                                <td>{{ $value->name ?? '' }}</td>
                                                <td>{{ $value->timer/60 }} menit</td>
                                                <td>{{ date('d/m/Y', strtotime($value->start_at)) }}</td>
                                                <td>{{ date('d/m/Y', strtotime($value->due_at)) }}</td>
                                                <td>
                                                    @if($value->name == 'ESSAY')
                                                    <a href="{{ route('soal.showEssay', [$kelas->slug, $value->id]) }}"><i class="feather icon-eye" title="Lihat Soal"></i></a>
                                                    @else
                                                    <a href="{{ route('kelas.soal.show', [$kelas->slug, $value->id]) }}"><i class="feather icon-eye" title="Lihat Soal"></i></a>
                                                    @endif
                                                    <span class="btn-modal" style="cursor: pointer;" data-href="{{ route('kelas.soal.edit', [$kelas->slug, $value->id]) }}"><i class="feather icon-edit" title="Edit"></i></span>
                                                    <span class="action-delete" style="cursor: pointer;" data-href="{{ route('kelas.soal.destroy', [$kelas->slug, $value->id]) }}"><i class="feather icon-trash" title="Delete"></i></span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Data list view end -->

    <div class="modal fade action-modal" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true"></div>

</div>
@endsection

@section('js')
    <script>
        $('.datatable').DataTable();

        $('.datatable').on('click', '.btn-modal', function(e){
            var t = $('.action-modal');
            $.ajax({
                url: $(this).data('href'),
                dataType: "html",
                success: function(e) {
                    $(t).html(e).modal("show")
                }
            })
        })

        $('.datatable').on('click', '.action-delete', function(e){
            var btn = $(this);
            e.stopPropagation();
            Swal.fire({
                title: 'Anda yakin?',
                text: "Anda akan menghapus data ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: btn.data('href'),
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        success: function(res) {
                            if(res.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: res.message
                                }).then((result) => {
                                    btn.closest('td').parent('tr').fadeOut();
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: res.message
                                })
                            }
                        }
                    })
                }
            })
        });
    </script>
@endsection