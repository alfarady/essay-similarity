<div class="modal-dialog" role="document">
    {!! Form::open(['url' => route('join.store'), 'method' => 'post']) !!}
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Gabung ke kelas</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                {!! Form::label('code', 'Kode Kelas') !!}
                {!! Form::text('code', null, ['class' => 'form-control', 'required', 'placeholder' => 'YZrcPQa', 'oninvalid' => "this.setCustomValidity('Mohon diisi dengan lengkap')", 'oninput' => "this.setCustomValidity('')"]) !!}
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Gabung</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
