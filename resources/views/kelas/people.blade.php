@extends('layouts.app')

@section('header')
    @include('kelas.partials.header')
@endsection

@section('content')
<div class="row d-flex justify-content-center">
  <div class="col-lg-9 col-12">
    <div class="card">
        <div class="card-header">
            <h1 class="text-primary">Teachers</h1>
        </div>
        <hr class="bg-primary">
        <div class="card-body">
          @foreach ($guru as $g)
          <span class="d-block"><img class="round" src="{{ $g->image ? asset('uploads/images/'.$g->image) : asset('uploads/images/profile.png') }}" alt="avatar" height="40" width="40"><p class="d-inline text-dark text-bold-500 ml-2">{{ $g->name ?? '' }}</p></span>
          @if($guru->count() > 1) <hr> @endif
          @endforeach
        </div>  
        <div class="card-header">
            <div class="float-left">
              <h1 class="text-primary">Classmates</h1>
            </div>
            <div class="float-right">
              <h6 class="text-primary text-small">{{ $siswa->count() }} Student</h6>
            </div>
        </div>
        <hr class="bg-primary">
        <div class="card-body">
          @foreach ($siswa as $s)
          <span class="d-block"><img class="round" src="{{ $s->image ? asset('uploads/images/'.$s->image) : asset('uploads/images/profile.png') }}" alt="avatar" height="40" width="40"><p class="d-inline text-dark text-bold-500 ml-2">{{ $s->name ?? '' }}</p></span>
          @if($siswa->count() > 1) <hr> @endif
          @endforeach
        </div>  
    </div>
  </div>
</div>
@endsection