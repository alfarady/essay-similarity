<div class="dropdown">
  <a class="list-group-item list-group-item-action border-0" type="button" id="classwork" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="font-medium-5 feather icon-folder mr-50"></i> Classwork</a>
  <div class="dropdown-menu" aria-labelledby="classwork">
      <a href="{{ route('kelas.task.index', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-book mr-50"></i> TUGAS</a>
      @if(!auth()->user()->hasRole('student'))
      <div class="dropdown-divider"></div>
      <a href="{{ route('kelas.soal.index', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-clipboard mr-50"></i> SOAL</a>
      @else
      <div class="dropdown-divider"></div>
      <a href="{{ route('kelas.soal.index', [$kelas->slug, 'type' => 'ESSAY']) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-circle mr-50"></i> ESSAY</a>
      <div class="dropdown-divider"></div>
      <a href="{{ route('kelas.soal.index', [$kelas->slug, 'type' => 'QUIZ']) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-circle mr-50"></i> QUIZ</a>
      <div class="dropdown-divider"></div>
      <a href="{{ route('kelas.soal.index', [$kelas->slug, 'type' => 'UTS']) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-circle mr-50"></i> SOAL UTS</a>
      <div class="dropdown-divider"></div>
      <a href="{{ route('kelas.soal.index', [$kelas->slug, 'type' => 'UAS']) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-circle mr-50"></i> SOAL UAS</a>
      @endif
  </div>
</div>