<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse d-flex align-items-center justify-content-between" id="navbar-mobile">
                @php
                    $role = auth()->user()->getRoleNames()[0];
                    $role = ($role == 'admin') ? 'Admin' : (($role == 'mentor') ? 'Guru' : 'Siswa');
                @endphp
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                    </ul>
                    <ul class="nav nav-tabs justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{ !request()->is('kelas/'.$kelas->slug.'/people') ? 'active' : '' }}" id="home-tab-center" href="{{ route('home', [$kelas->slug]) }}" aria-controls="home-center" role="tab" aria-selected="true">Stream</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('kelas/'.$kelas->slug.'/people') ? 'active' : '' }}" id="service-tab-center" href="{{ route('kelas.people', [$kelas->slug]) }}" aria-controls="service-center" role="tab" aria-selected="false">People</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        @include('layouts.partials.notification')
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{ auth()->user()->name ?? '' }}</span><span class="user-status">{{ $role ?? '' }}</span></div><span><img class="round" src="{{ auth()->user()->image ? asset('uploads/images/'.auth()->user()->image) : asset('uploads/images/profile.png') }}" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ route('home.setting') }}"><i class="fa fa-cog"></i> Pengaturan</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="feather icon-power"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
            </div>
        </div>
    </div>
</nav>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>