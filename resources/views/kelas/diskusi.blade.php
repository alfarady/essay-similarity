@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/users.css') }}">
@endsection

@section('header')
    @include('kelas.partials.header')
@endsection

@section('content')
<div class="content-header row">
</div>
<div class="content-body">
    <div id="user-profile">
        <div class="row">
            <div class="col-12">
                <div class="profile-header mb-2">
                    <div class="relative">
                        <div class="cover-container">
                            <img class="img-fluid bg-cover rounded-0 w-100" style="height: 20rem" src="{{ asset('assets/images/smkn1banner.jpg') }}" alt="User Profile Image">
                            <div class="card-img-overlay overflow-hidden">
                                <h1 class="card-text text-white ml-3" style="font-size: 3rem">{{ $kelas->name ?? '' }}</h1>
                            </div>
                        </div>
                        <div class="profile-img-container d-flex align-items-center justify-content-between">
                            <img style="background: white" src="{{ auth()->user()->image ? asset('uploads/images/'.auth()->user()->image) : asset('uploads/images/profile.png') }}" class="rounded-circle img-border box-shadow-1" alt="Card image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section id="profile-info">
            <div class="row mt-3">
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <h4>MENU</h4>
                        </div>
                        <div class="list-group list-group-filters font-medium-1">
                            <a href="{{ route('kelas.diskusi', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-message-circle mr-50"></i> Diskusi</a>
                            <div class="dropdown">
                                <a class="list-group-item list-group-item-action border-0" type="button" id="materi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="font-medium-5 feather icon-file mr-50"></i> Materi</a>
                                <div class="dropdown-menu" aria-labelledby="materi">
                                    <a href="{{ route('kelas.materi.index', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-file mr-50"></i> PPT/PDF</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="{{ route('kelas.video.index', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-film mr-50"></i> VIDEO</a>
                                </div>
                            </div>
                            @include('kelas.partials.dropdown')
                            <a href="{{ route('report.nilai', [$kelas->slug]) }}" class="list-group-item list-group-item-action border-0"><i class="font-medium-5 feather icon-server mr-50"></i> Hasil Kinerja</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-12">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(['url' => route('home.feed', [$kelas->slug]), 'method' => 'post', 'files' => true]) !!}
                            <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                            <fieldset class="form-label-group mb-50">
                                <textarea name="message" class="form-control" id="label-textarea" rows="3" placeholder="Apa yang anda pikirkan?"></textarea>
                                <label for="label-textarea">Apa yang anda pikirkan?</label>
                            </fieldset>
                            <fieldset class="form-label-group mb-50">
                                <input type="file" accept="image/*" name="image" class="form-control">
                            </fieldset>
                            <button type="submit" class="btn btn-sm btn-primary">Kirim</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @foreach ($feeds as $value)
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-start align-items-center mb-1">
                                <div class="avatar mr-1">
                                    <img src="{{ $value->user->image ? asset('uploads/images/'.$value->user->image) : asset('uploads/images/profile.png') }}" alt="avtar img holder" height="45" width="45">
                                </div>
                                <div class="user-page-info">
                                    <p class="mb-0">{{ $value->user->name }}</p>
                                    <span class="font-small-2">{{ date('d M Y H:i', strtotime($value->created_at)) }}</span>
                                </div>
                            </div>
                            <p>{{ $value->message }}</p>
                            @if ($value->image)
                            <img class="img-fluid card-img-top rounded-sm mb-2" src="{{ asset('uploads/images/'.$value->image) }}" alt="avtar img holder">
                            @endif
                            <div class="d-flex justify-content-start align-items-center mb-1">
                                <div>
                                    <b>Komentar</b>
                                </div>
                                <p class="ml-auto d-flex align-items-center">
                                    <i class="feather icon-message-square font-medium-2 mr-50"></i>{{ count($value->replies) }}
                                </p>
                            </div>
                            @foreach ($value->replies as $reply)
                                <div class="d-flex justify-content-start align-items-center mb-1">
                                    <div class="avatar mr-50">
                                        <img src="{{ $reply->user->image ? asset('uploads/images/'.$reply->user->image) : asset('uploads/images/profile.png') }}" alt="Avatar" height="30" width="30">
                                    </div>
                                    <div class="user-page-info">
                                        <h6 class="mb-0">{{ $reply->user->name }}</h6>
                                        <span class="font-small-2">{{ $reply->message }}</span>
                                    </div>
                                    <div class="ml-auto cursor-pointer">
                                        <span class="font-small-2">{{ $reply->created_at->diffForHumans() }}</span>
                                    </div>
                                </div>
                            @endforeach
                            {!! Form::open(['url' => route('home.reply', [$kelas->slug]), 'method' => 'post']) !!}
                            {!! Form::hidden('feed_id', $value->id) !!}
                            <fieldset class="form-label-group mb-50">
                                <textarea name="message" class="form-control" id="label-textarea" rows="1" placeholder="Tulis Komentar"></textarea>
                                <label for="label-textarea">Tulis Komentar</label>
                            </fieldset>
                            <button type="submit" class="btn btn-sm btn-primary">Kirim</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade action-modal" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true"></div>
@endsection

@section('js')
    <script src="{{ asset('app-assets/js/scripts/pages/user-profile.js') }}"></script>
    <script>
        $('.action-show').on("click",function(e){
            e.stopPropagation();
            const link = $(this).data('link');
            $.ajax({
                url: $(this).data('href'),
                dataType: "json",
                success: function(data) {
                    window.open(link,'_blank');
                }
            })
        });
    </script>
    <script>
        $('.datatable').on('click', '.btn-modal', function(e){
            var t = $('.action-modal');
            $.ajax({
                url: $(this).data('href'),
                dataType: "html",
                success: function(e) {
                    $(t).html(e).modal("show")
                }
            })
        })
    </script>
@endsection
