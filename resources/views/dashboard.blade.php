@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/users.css') }}">
@endsection

@section('header')
    @include('layouts.partials.header')
@endsection

@section('content')
<div class="content-header row">
</div>
<div class="content-body">
    <div id="user-profile">
        <section id="profile-info">
            <div class="row">
                @foreach ($data as $value)
                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="relative">
                                <div class="cover-container">
                                    <img class="img-fluid bg-cover rounded-0 w-100" style="height: 7rem" src="{{ asset('app-assets/images/backgrounds/kuning.jpg') }}" alt="User Profile Image">
                                    <div class="card-img-overlay overflow-hidden">
                                        <h2 class="card-text text-white">{{ $value->subject ?? '' }}</h2>
                                        @foreach ($guru as $g)
                                        <h6 class="card-text text-white text-small">{{ $g->name ?? '' }} - {{ $value->name ?? '' }}</h6>
                                        @endforeach
                                    </div>
                                </div>
                                @php
                                $photo = count($guru) > 0 ? $guru[0]->image : '';
                                @endphp
                                <div class="profile-img-container d-flex align-items-center justify-content-between">
                                    <div></div>
                                    <div class="float-right">
                                        <img style="background: white" src="{{ $photo ? asset('uploads/images/'.$photo) : asset('uploads/images/profile.png') }}" class="rounded-circle img-border box-shadow-1" alt="Card image">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-5 pb-3">
                                <hr>
                                <span class="float-right">
                                    <a href="{{ route('home', [$value->slug]) }}" class="card-link"><i class="fas fa-home fa-2x"></i></a>
                                    {{-- <a href="#" class="card-link"><i class="fas fa-folder-open fa-2x"></i></a> --}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
    </div>
</div>
<div class="modal fade action-modal" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true"></div>
@endsection

@section('js')
    <script src="{{ asset('app-assets/js/scripts/pages/user-profile.js') }}"></script>
    <script>
        $('.datatable').on('click', '.btn-modal', function(e){
            var t = $('.action-modal');
            $.ajax({
                url: $(this).data('href'),
                dataType: "html",
                success: function(e) {
                    $(t).html(e).modal("show")
                }
            })
        })
    </script>
@endsection
