<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    protected $guarded = ['id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    public function soal()
    {
        return $this->belongsTo(Soal::class);
    }

    public function soal_item()
    {
        return $this->belongsTo(SoalItem::class);
    }
}
