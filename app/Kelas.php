<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $guarded = ['id'];

    public function siswa()
    {
        return $this->belongsToMany(User::class, 'kelas_siswa', 'kelas_id', 'siswa_id')->withTimestamps();
    }

    public function guru()
    {
        return $this->belongsToMany(User::class, 'guru_kelas', 'kelas_id', 'guru_id')->withTimestamps();
    }
}
