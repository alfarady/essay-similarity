<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use App\SoalItem;
use App\Nilai;
use App\ExamAnswer;

class ProcessOnFinished implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $answers;
    protected $user;
    protected $kelas;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $kelas, $answers)
    {
        $this->answers = $answers;
        $this->user = $user;
        $this->kelas = $kelas;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $nilai = 0;

            foreach($this->answers as $key => $answer) {
                $soal_item = SoalItem::findOrFail($answer->soal_item_id);
                $txtAnswer = $answer->answer;
                $txtKey = $soal_item->kj;

                $response = Http::timeout(86400)->post('https://kamusbasdat.sadean.app/scoring', [
                    'answer' => $txtAnswer,
                    'key' => $txtKey,
                ])->throw()->json();

                $score = (float) $response['data'];
                $human_score = 0;

                $eAnswer = ExamAnswer::findOrFail($answer->id);
                $eAnswer->score = $score;
                $eAnswer->save();

                if($score >= 0 && $score <= 0.25) $human_score = 0;
                else if($score > 0.25 && $score <= 0.5) $human_score = 1;
                else if($score > 0.5 && $score <= 0.75) $human_score = 2;
                else if($score > 0.75 && $score <= 1) $human_score = 3;

                $nilai += $human_score;
            }

            if(count($this->answers) > 0) {
                $nilai = $nilai/count($this->answers);
            } else {
                $nilai = 0;
            }

            $user = Nilai::where('siswa_id', $this->user->id)->where('kelas_id', $this->kelas->id)->first();
            $old_nilai = json_decode($user->nilai, TRUE);
            $old_nilai[strtolower('ESSAY')] = $nilai;
            $user->nilai = json_encode($old_nilai);
            $user->save();
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
