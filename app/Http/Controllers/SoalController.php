<?php

namespace App\Http\Controllers;

use App\Soal;
use App\SoalItem;
use App\Exam;
use App\ExamAnswer;
use App\Jobs\ProcessOnFinished;
use App\Kelas;
use App\Nilai;
use DB;
use Illuminate\Http\Request;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Kelas $kelas)
    {
        $data = Soal::where('kelas_id', '=', $kelas->id)->get();

        if(auth()->user()->hasRole('student')) {
            $type = request()->type ?? 'UTS';

            $exam_done_ids = Exam::where([
                'siswa_id' => auth()->user()->id,
                'state' => 'done'
            ])->pluck('soal_id');

            $data = Soal::where('kelas_id', '=', $kelas->id)->whereNotIn('id', $exam_done_ids)->where('name', $type)->first();

            if($data) {
                $data->exam_state = 'none';
                $data->timer_ori = $data->timer;
                $data->is_permitted = $type == 'QUIZ' || $type == 'UTS' || $type == 'ESSAY' ? true : $this->calculateNilai($kelas);

                $exist_exam = Exam::where([
                    'soal_id' => $data->id,
                    'siswa_id' => auth()->user()->id
                ])->first();

                if($exist_exam) {
                    $diff = strtotime($exist_exam->expired_at) - strtotime(date('Y-m-d H:i:s'));
                    $data->timer = $diff;
                    $data->exam_state = 'start';

                    if($diff <= 0) {
                        $exist_exam->state = 'done';
                        $exist_exam->save();
                        $data->timer = $diff;
                        $data->exam_state = 'done';
                    }
                }
            } else {
                $exist_exam = null;
            }
            // dd($data);
            return view('soal.student', compact('data', 'exist_exam', 'kelas'));
        }

        return view('soal.index', compact('data', 'kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Kelas $kelas)
    {
        return view('soal.create', compact('kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Kelas $kelas)
    {
        $input = $request->all();
        $input['timer'] = $input['timer']*60;

        if(request()->name == 'ESSAY') $input['size_choices'] = 1;

        $soal = Soal::create($input);

        activity()
            ->performedOn($soal)
            ->causedBy(auth()->user())
            ->withProperties(['kelas' => $kelas->id])
            ->log(':causer.name menambahkan soal :subject.name');

        flash('Berhasil menambahkan soal')->success();

        if(request()->name == 'ESSAY') return redirect()->route('soal.showEssay', [$kelas->slug, $soal->id]);

        return redirect()->route('kelas.soal.show', [$kelas->slug, $soal->id]);
    }

    public function storeItem(Request $request, Kelas $kelas, $id)
    {
        $item_data = $request->all();
        $items = [];
        $soal = Soal::find($id);

        if(array_key_exists('old', $item_data)) {
            foreach($item_data['old'] as $id_item => $item) {
                if($soal->name != 'ESSAY') {
                    $olds = [
                        'soal_id' => $id,
                        'nomor' => $item['nomor'],
                        'question' => $item['soal'],
                        'choices' => json_encode($item['choices']),
                        'kj' => $item['kj']
                    ];
                } else {
                    $olds = [
                        'soal_id' => $id,
                        'nomor' => $item['nomor'],
                        'question' => $item['soal'],
                        'choices' => json_encode([]),
                        'kj' => $item['kj']
                    ];
                }

                SoalItem::find($id_item)->update($olds);
            }
        }

        if(array_key_exists('item', $item_data)) {
            foreach($item_data['item'] as $key => $item) {
                if($soal->name != 'ESSAY') {
                    $items[] = [
                        'soal_id' => $id,
                        'nomor' => $item['nomor'],
                        'question' => $item['soal'],
                        'choices' => json_encode($item['choices']),
                        'kj' => $item['kj']
                    ];
                } else {
                    $items[] = [
                        'soal_id' => $id,
                        'nomor' => $item['nomor'],
                        'question' => $item['soal'],
                        'choices' => json_encode([]),
                        'kj' => $item['kj']
                    ];
                }
            }
        }

        if(count($items) > 0) {
            SoalItem::insert($items);
        }

        flash('Berhasil menyimpan butir soal')->success();

        return redirect()->route('kelas.soal.index', [$kelas->slug]);
    }

    public function storeAnswer(Request $request)
    {
        try {
            $input = $request->all();
            $input['siswa_id'] = auth()->user()->id;
            $input['soal_item_id'] = $input['id'];

            $soal_item = SoalItem::findOrFail($input['id']);
            $soal = Soal::find($input['soal_id']);

            $input['exam_id'] = Exam::where([
                'siswa_id' => auth()->user()->id,
                'soal_id' => $soal_item->soal_id
            ])->first()->id;

            if($soal->name != 'ESSAY'){
                if(array_key_exists('answer', $input)) {
                    if($input['answer'] == $soal_item->kj)
                        $input['is_true'] = true;
                    else
                        $input['is_true'] = false;
                } else {
                    $input['is_true'] = false;
                }
            }
            $input['is_true'] = true;

            $old_answer = ExamAnswer::where([
                'siswa_id' => auth()->user()->id,
                'soal_item_id' => $input['id'],
                'soal_id' => $input['soal_id']
            ])->first();

            if($old_answer) {
                $old_answer->update($input);
            } else {
                ExamAnswer::create($input);
            }

            return response()->json([
                'status' => true,
                'message' => 'Berhasil menyimpan jawaban'
            ]);
        } catch(\Exception $e) {
            dd($e);
            return response()->json([
                'status' => false,
                'message' => 'Gagal menyimpan jawaban'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas, $id)
    {
        $data = Soal::find($id);
        return view('soal.show', compact('data', 'kelas'));
    }

    public function essay(Kelas $kelas, $id)
    {
        $data = Soal::find($id);
        return view('soal.essay', compact('data', 'kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas, $id)
    {
        $data = Soal::find($id);
        return view('soal.edit', compact('data', 'kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kelas $kelas, $id)
    {
        $input = $request->all();
        $input['timer'] = $input['timer']*60;

        Soal::find($id)->update($input);

        flash('Berhasil mengedit soal')->success();

        return redirect()->route('kelas.soal.index', [$kelas->slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kelas $kelas, $id)
    {
        try {
            Soal::find($id)->delete();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil menghapus soal'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal menghapus soal'
            ]);
        }
    }

    public function examStart(Kelas $kelas, $id)
    {
        try {
            $soal = Soal::findOrFail($id);

            $expired_at = strtotime(date('Y-m-d H:i:s')) + $soal->timer;
            $expired_at = date('Y-m-d H:i:s', $expired_at);

            $exam = Exam::create([
                'siswa_id' => auth()->user()->id,
                'soal_id' => $id,
                'expired_at' => $expired_at
            ]);

            activity()
                ->performedOn($soal)
                ->causedBy(auth()->user())
                ->withProperties(['kelas' => $kelas->id])
                ->log(':causer.name memulai timer ujian :subject.name');

            return response()->json([
                'status' => true,
                'message' => 'Berhasil memulai ujian',
                'exam_id' => $exam->id
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal memulai ujian'
            ]);
        }
    }

    public function examFinish(Request $request, Kelas $kelas)
    {
        try {
            DB::beginTransaction();

            $exam = Exam::findOrFail($request->id);
            $exam->update(['state' => 'done']);
            $soal = Soal::findOrFail($exam->soal_id);

            if(!$soal->name != 'ESSAY'){
                $count_soal = $soal->size_item;
                $count_true = ExamAnswer::where([
                    'siswa_id' => auth()->user()->id,
                    'exam_id' => $request->id,
                    'soal_id' => $soal->id,
                    'is_true' => true
                ])->count();
                $nilai = ($count_true/$count_soal) * 100;
            }

            $nilai = 0;

            // $user = auth()->user();
            $user = Nilai::where('siswa_id', auth()->user()->id)->where('kelas_id', $kelas->id)->first();
            $old_nilai = json_decode($user->nilai, TRUE);
            $old_nilai[strtolower($soal->name)] = $nilai;
            $user->nilai = json_encode($old_nilai);
            $user->save();

            activity()
                ->performedOn($soal)
                ->causedBy(auth()->user())
                ->withProperties(['kelas' => $kelas->id])
                ->log(':causer.name menyelesaikan timer ujian :subject.name');

            //QUEUE
            if($soal->name == 'ESSAY'){
                ProcessOnFinished::dispatch(auth()->user(), $kelas, $exam->answers);
            }

            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil menyelesaikan ujian'
            ]);
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => 'Gagal menyelesaikan ujian'
            ]);
        }
    }

    private function calculateNilai(Kelas $kelas)
    {
        // $user = auth()->user();
        $user = Nilai::where('siswa_id', auth()->user()->id)->where('kelas_id', $kelas->id)->first();
        $nilai = json_decode($user->nilai, TRUE);

        $meanTugas = ($nilai['tugas_1'] + $nilai['tugas_2'] + $nilai['tugas_3'] + $nilai['tugas_4']) / 4;
        $nilaiUts = $nilai['uts'];

        if($meanTugas >= 75 && $nilaiUts >= 80) {
            return true;
        } else {
            return false;
        }
    }
}
