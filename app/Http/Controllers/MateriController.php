<?php

namespace App\Http\Controllers;

use App\Kelas;
use App\Materi;

use Illuminate\Http\Request;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Kelas $kelas)
    {
        $data = Materi::where('kelas_id', '=', $kelas->id)->get();
        return view('materi.index', compact('data', 'kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Kelas $kelas)
    {
        return view('materi.create', compact('kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Kelas $kelas)
    {
        $input = $request->all();

        if ($request->hasFile('file')) {
            $input['file'] = time().'_'.request()->file->getClientOriginalName();
            
            request()->file->move(public_path('uploads/file/'), $input['file']);
        }
        
        $data = Materi::create($input);

        activity()
            ->performedOn($data)
            ->causedBy(auth()->user())
            ->withProperties(['kelas' => $kelas->id])
            ->log(':causer.name menambahkan materi :subject.name');

        flash('Berhasil menambahkan materi')->success();

        return redirect()->route('kelas.materi.index', [$kelas->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas, $id)
    {
        $data = Materi::find($id);

        activity()
            ->performedOn($data)
            ->causedBy(auth()->user())
            ->withProperties(['kelas' => $kelas->id])
            ->log(':causer.name mendownload materi :subject.name');

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas, $id)
    {
        $data = Materi::find($id);
        return view('materi.edit', compact('data', 'kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kelas $kelas, $id)
    {
        $input = $request->all();

        if ($request->hasFile('file')) {
            $input['file'] = time().'_'.request()->file->getClientOriginalName();
            
            request()->file->move(public_path('uploads/file/'), $input['file']);
        }
        
        Materi::find($id)->update($input);

        flash('Berhasil mengedit materi')->success();

        return redirect()->route('kelas.materi.index', [$kelas->slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kelas $kelas, $id)
    {
        try {
            Materi::find($id)->delete();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil menghapus materi'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal menghapus materi'
            ]);
        }
    }
}
