<?php

namespace App\Http\Controllers;

use App\Kelas;
use App\Nilai;
use App\User;
use App\ExamAnswer;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class ReportController extends Controller
{
    public function activity(Kelas $kelas)
    {
        $siswa = User::role('student')->with('kelas_siswa')
                ->whereHas('kelas_siswa', function($q) use ($kelas) {
                    $q->where('kelas_siswa.kelas_id', '=', $kelas->id);
                })->get();

        $guru = User::role('mentor')->with('kelas_guru')
                ->whereHas('kelas_guru', function($q) use ($kelas) {
                    $q->where('guru_kelas.kelas_id', '=', $kelas->id);
                })->get();

        $condition = [];
        foreach ($siswa as $value) {
            $condition[] = $value->id;
        }
        foreach ($guru as $value) {
            $condition[] = $value->id;
        }

        $data = Activity::where('properties->kelas', $kelas->id)->whereIn('causer_id', $condition)->latest()->get();

        return view('report.activity', compact('data'));
    }

    public function nilai(Kelas $kelas)
    {
        $siswa = User::role('student')->with('kelas_siswa')
                ->whereHas('kelas_siswa', function($q) use ($kelas) {
                    $q->where('kelas_siswa.kelas_id', '=', $kelas->id);
                })->get();
        $condition = [];
        foreach ($siswa as $value) {
            $condition[] = $value->id;
        }
        $data = Nilai::where('kelas_id', $kelas->id)->whereIn('siswa_id', $condition)->get();

        if(auth()->user()->hasRole('student'))
            // $data = [auth()->user()];
            $data = Nilai::where('siswa_id', auth()->user()->id)->where('kelas_id', $kelas->id)->get();

        return view('report.nilai', compact('data', 'kelas'));
    }

    public function nilaiDetail(Kelas $kelas, $id)
    {
        $siswa = User::findOrFail($id);
        $data = ExamAnswer::where([
            'siswa_id' => $siswa->id
        ])->with(['exam', 'soal', 'soal_item'])->get();

        return view('report.detail-nilai', compact('data', 'siswa', 'kelas'));
    }

    public function saveNilai(Kelas $kelas)
    {
        try {
            $input = request()->all();

            // $user = User::find($input['id']);
            $user = Nilai::find($input['id']);

            $nilai = json_decode($user->nilai, TRUE);
            $nilai[$input['index']] = $input['nilai'];

            $user->nilai = json_encode($nilai);
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil menyimpan nilai'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal menyimpan nilai'
            ]);
        }
    }
}
