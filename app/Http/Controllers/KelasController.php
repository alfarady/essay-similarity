<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Kelas;
use App\Nilai;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kelas::all();
        return view('kelas.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['slug'] = Str::slug(request('name')).'-'.Str::random(10);
        $input['code'] = Str::random(7);

        $kelas = Kelas::create($input);
        $kelas->guru()->attach(auth()->user()->id);

        flash('Berhasil menambahkan kelas')->success();

        if(auth()->user()->hasRole('admin'))
            return redirect()->route('kelas.index');

        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas)
    {
        return redirect()->route('home', [$kelas->slug]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas)
    {
        $data = $kelas;
        return view('kelas.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kelas $kelas)
    {
        $input = $request->all();

        $kelas->update($input);

        flash('Berhasil mengedit kelas')->success();

        return redirect()->route('kelas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kelas $kelas)
    {
        try {
            $kelas->delete();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil menghapus kelas'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal menghapus kelas'
            ]);
        }
    }

    public function cjoin()
    {
        return view('kelas.join');
    }

    public function join(Request $request)
    {
        $code = $request->code;

        $kelas = Kelas::where('code', '=', $code)->first();
        if(!$kelas) {
            flash('Gagal gabung kelas! Periksa kembali kode anda.')->error();
            return redirect()->route('dashboard');
        }

        $is_join = false;

        if(!auth()->user()->hasRole('student'))
            foreach (auth()->user()->kelas_guru as $related_kelas) {
                if($related_kelas->pivot->kelas_id == $kelas->id) {
                    $is_join = true;
                }
            }

        foreach (auth()->user()->kelas_siswa as $related_kelas) {
            if($related_kelas->pivot->kelas_id == $kelas->id) {
                $is_join = true;
            }
        }


        if($is_join) {
            flash('Anda telah tergabung di kelas ini.')->warning();
            return redirect()->route('dashboard');
        }

        if(!auth()->user()->hasRole('student')){
            $kelas->guru()->attach(auth()->user()->id);
        }
        else{
            $kelas->siswa()->attach(auth()->user()->id);
            Nilai::create([
                'siswa_id' => auth()->user()->id,
                'kelas_id' => $kelas->id,
                'nilai' => json_encode([
                    'tugas_1' => 0,
                    'tugas_2' => 0,
                    'tugas_3' => 0,
                    'tugas_4' => 0,
                    'essay' => 0,
                    'uas' => 0,
                    'uts' => 0
                ])
            ]);
        }

        flash('Berhasil gabung kelas.')->success();

        return redirect()->route('dashboard');
    }

    public function diskusi(Kelas $kelas)
    {
        $feeds = Feed::where('kelas_id', '=', $kelas->id)->orderBy('created_at', 'desc')->get();

        return view('kelas.diskusi', compact('feeds', 'kelas'));
    }

    public function people(Kelas $kelas)
    {
        $siswa = User::role('student')->with('kelas_siswa')
                ->whereHas('kelas_siswa', function($q) use ($kelas) {
                    $q->where('kelas_siswa.kelas_id', '=', $kelas->id);
                })->get();

        $guru = User::role('mentor')->with('kelas_guru')
                ->whereHas('kelas_guru', function($q) use ($kelas) {
                    $q->where('guru_kelas.kelas_id', '=', $kelas->id);
                })->get();
        // dd($guru);

        return view('kelas.people', compact('kelas', 'siswa', 'guru'));
    }
}
