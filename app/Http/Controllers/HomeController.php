<?php

namespace App\Http\Controllers;

use App\Materi;
use App\Video;
use App\Feed;
use App\FeedReply;
use App\Kelas;
use App\User;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Kelas::all();
        
        if(auth()->user()->hasRole('mentor'))
            $data = Kelas::with('guru')
                    ->whereHas('guru', function($q) {
                        $q->where('guru_kelas.guru_id', '=', auth()->user()->id);
                    })->get();

        if(auth()->user()->hasRole('student'))
            $data = Kelas::with('siswa')
                    ->whereHas('siswa', function($q) {
                        $q->where('kelas_siswa.siswa_id', '=', auth()->user()->id);
                    })->get();

        $condition = [];
        foreach ($data as $value) {
            $condition[] = $value->id;
        }

        $guru = User::role('mentor')->with('kelas_guru')
                ->whereHas('kelas_guru', function($q) use ($condition) {
                    $q->whereIn('guru_kelas.kelas_id', $condition);
                })->take(1)->get();
        
        return view('dashboard', compact('data', 'guru'));
    }

    public function home(Kelas $kelas)
    {
        $feeds = Feed::where('kelas_id', '=', $kelas->id)->orderBy('created_at', 'desc')->get();
        $materi = Materi::where('kelas_id', '=', $kelas->id)->orderBy('created_at', 'desc')->take(5)->get();
        $video = Video::where('kelas_id', '=', $kelas->id)->orderBy('created_at', 'desc')->take(5)->get();
        $siswa = User::role('student')->with('kelas_siswa')
                ->whereHas('kelas_siswa', function($q) use ($kelas) {
                    $q->where('kelas_siswa.kelas_id', '=', $kelas->id);
                })->get();

        $guru = User::role('mentor')->with('kelas_guru')
                ->whereHas('kelas_guru', function($q) use ($kelas) {
                    $q->where('guru_kelas.kelas_id', '=', $kelas->id);
                })->get();
        
        $condition = [];
        foreach ($siswa as $value) {
            $condition[] = $value->id;
        }
        foreach ($guru as $value) {
            $condition[] = $value->id;
        }
        $data = Activity::where('properties->kelas', $kelas->id)->whereIn('causer_id', $condition)->latest()->take(5)->get();

        return view('home', compact('feeds', 'materi', 'video', 'kelas', 'data'));
    }

    public function setting()
    {
        $data = auth()->user();
        return view('setting.index', compact('data'));
    }

    public function updateSetting(Request $request)
    {
        $user = auth()->user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->nomor_induk = $request->nomor_induk;

        if ($request->hasFile('image')) {
            $user->image = time().'.'.request()->image->getClientOriginalExtension();
            
            request()->image->move(public_path('uploads/images/'), $user->image);
        }

        if($user->hasRole('student')) {
            $user->ttl = $request->ttl;
            $user->phone = $request->phone;
        } else {
            $user->jabatan = $request->jabatan;
            $user->address = $request->address;
        }

        $user->save();

        flash('Berhasil menyimpan pengaturan')->success();

        return redirect()->route('home.setting');
    }

    public function updatePassword(Request $request)
    {
        if($request->password != $request->con_password) {
            flash('Konfirmasi password tidak sama')->error();
            return redirect()->route('home.setting');
        }

        $credentials = [
            'email' => auth()->user()->email,
            'password' => $request->old_password
        ];

        if(!\Auth::attempt($credentials)) {
            flash('Password lama anda salah')->error();
            return redirect()->route('home.setting');
        }

        $user = auth()->user();
        $user->password = \Hash::make($request->password);
        $user->save();
        
        flash('Berhasil merubah password')->success();
        return redirect()->route('home.setting');
    }

    public function loadMoreNotifications()
    {
        $notifications = auth()->user()->notifications()->orderBy('created_at', 'DESC')->paginate(10);

        auth()->user()->unreadNotifications->markAsRead();

        $notifications_data = [];
        foreach ($notifications as $notification) {
            $action = $notification->data['action'];
            $type = $notification->data['type'];
            $detail = json_decode($notification->data['detail'], TRUE);

            $title = '';
            $msg = '';
            $icon_class = '';
            $text_class = '';
            if ($type == 'promo') {
                $title = 'Ada Promo Baru nih!';
                $msg = $detail['message'];
                $icon_class = 'feather icon-plus-square';
                $text_class = 'primary';
            } else if($type == 'stock') {
                $title = 'Stok produk '.$detail['name'].' kosong';
                $msg = 'Silahkan tambahkan stok';
                $icon_class = ' feather icon-alert-triangle';
                $text_class = 'danger';
            } else if($type == 'order') {
                if($action == 'new') {
                    $title = 'Ada pesanan baru masuk';
                    $msg = 'Pesanan baru dari meja '.$detail['table'];
                    $icon_class = ' feather icon-plus-square';
                    $text_class = 'primary';
                } else if($action == 'sell') {
                    $title = 'Penjualan '.$detail['ref_no'].' berhasil dilakukan';
                    $msg = 'Penjualan pada meja '.$detail['table'].' sejumlah Rp '.$detail['total'].' berhasil dilakukan';
                    $icon_class = ' feather icon-check-circle';
                    $text_class = 'info';
                } else if($action == 'pay') {
                    $title = 'Pembayaran berhasil dilakukan';
                    $msg = 'Pembayaran pada penjualan '.$detail['ref_no'].' sejumlah Rp '.$detail['total'].' berhasil dibayarkan';
                    $icon_class = ' fa fa-dollar-sign';
                    $text_class = 'success';
                }
            }

            $notifications_data[] = [
                'msg' => $msg,
                'title' => $title,
                'icon_class' => $icon_class,
                'text_class' => $text_class,
                'read_at' => $notification->read_at,
                'created_at' => $notification->created_at->diffForHumans()
            ];
        }

        return view('layouts.partials.notification_list', compact('notifications_data'));
    }

    public function feed(Request $request, Kelas $kelas)
    {
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;

        if ($request->hasFile('image')) {
            $input['image'] = time().'_'.request()->image->getClientOriginalName();
            
            request()->image->move(public_path('uploads/images/'), $input['image']);
        }

        Feed::create($input);

        return redirect()->route('kelas.diskusi', [$kelas->slug]);
    }

    public function reply(Request $request, Kelas $kelas)
    {
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;

        FeedReply::create($input);

        return redirect()->route('kelas.diskusi', [$kelas->slug]);
    }
}
